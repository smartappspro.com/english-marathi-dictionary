# English to Marathi Dictionary 

English to Marathi Dictionary with offline feature.
Download this largest marathi dictionary with over 1 lakhs of word meaning.
This Dictionary have favorite word marking to get useful word immediately.

This have 3 types of Game to test your Vocal Level.
1. Marathi to English Game
2. English to Marathi Game
3. Fill in the Blanks Game


English to Marathi Dictionary has a word of day feature to show daily word meanings for you with a notification.

You can turn on or off this feature anytime from the setting.

Features:

- Work Offline
- Bookmark Words
- Words of day for daily words
- Multiple Games
- Scoreboard for your knowledge level
- Listen to Word
- Know Word meaning type like Noun, Pronoun, Verb, Adverb, Adjective etc.
- View all words matching with a search keyword
- Settings for control Words of day and timing for notification

Download from Playstore

https://play.google.com/store/apps/details?id=com.smartappspro.marathidictionary
